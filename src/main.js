import { createApp } from 'vue';
import firebase from 'firebase';
import router from './router.js';
import App from './App.vue';
import store from './store/index.js'
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/js/all.js';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

// clientId: '618039887476-3lmt8135a2jfbjsr47hj4qb2puk14dfj.apps.googleusercontent.com',


// Initialize Firebase
firebase.initializeApp({
  apiKey: 'AIzaSyANxO2hHpmTDLnLtCFjawKVEGSMetOV9qM',
  authDomain: 'sendy-intern.firebaseapp.com',
  projectId: 'sendy-intern',
  storageBucket: 'sendy-intern.appspot.com',
  messagingSenderId: '404880296078',
  appId: '1:404880296078:web:f61302038057a63be5e1df',
  measurementId: 'G-TGWLN3YW9T'
});

const app = createApp(App)
 

firebase.auth().onAuthStateChanged(() => {
   app.use(store);
   app.use(router);
   app.mount('#app'); 
});



