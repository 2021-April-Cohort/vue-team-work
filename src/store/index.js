import { createStore } from 'vuex';
import internsModule from './modules/interns.js';


// Create store
const store = createStore({
  modules: {
    interns: internsModule,
  }
});
export default store;