import axios from 'axios';

const state = {
  Interns: [],
  intern:[]
   
};

const getters = {
  allInterns: state => state.interns,
  singleIntern: state => state.intern,
  
};

const actions = {
  //Fetch all  interns from the api
  async fetchInterns({ commit }) {
    const response = await axios.get(
      'https://sendy-interns-api-adonis.herokuapp.com/interns'
    );
    commit('setInterns', response.data);
  },

  async fetchIntern({ commit }, id) {
   const response = await axios.get(
     `https://sendy-interns-api-adonis.herokuapp.com/interns/${id}`
   );
   const {data} = response;
   console.log(data.data.intern.name);
    commit('setIntern', response.data);
  }
};

const mutations = {
  setInterns: (state, interns) => (state.interns = interns),
  setIntern: (state, intern) => (state.intern = intern)
};
export default {
  state,
  getters,
  actions,
  mutations
};
