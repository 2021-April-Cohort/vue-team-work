import { createRouter, createWebHistory } from 'vue-router';
import firebase from 'firebase';

import Landing from '@/pages/Landing.vue';
import Intern from '@/pages/Intern.vue';
import Interns from '@/pages/Interns.vue';
import Login from '@/pages/Login.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/home',
      component: Landing,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/interns',
      name: 'interns',
      props: true,
      component: Interns,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/intern/:id',
      name: 'intern',
      props: true,
      component: Intern,
      meta: {
        requiresAuth: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  if (requiresAuth && !currentUser) {
    next('login');
  } else if (!requiresAuth && currentUser) {
    next('home');
  } else {
    next();
  }
});

export default router;