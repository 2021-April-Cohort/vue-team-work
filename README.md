# Sendy Interns - Vue Application

This is a simple VueJS application that consume Adonis API

### Version: 1.0.0


| Image 1    | Image 2     | Image 3     |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1621036198/vue/screen1_g94tgs.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1621036198/vue/screen2_nczrwh.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1621036198/vue/screen3_r5n880.png" width="250"> |


### Frontend Link

Link: [https://sendy-intern.web.app/](https://sendy-intern.web.app/)


### Api EndPoints

GET: [https://sendy-interns-api-adonis.herokuapp.com/interns](https://sendy-interns-api-adonis.herokuapp.com/interns)

GET/ID: [https://sendy-interns-api-adonis.herokuapp.com/interns/:id](https://sendy-interns-api-adonis.herokuapp.com/interns/:id)

### Usage

```js
git clone  https://gitlab.com/2021-April-Cohort/vue-team-work


```
